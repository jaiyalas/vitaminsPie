# VitaminsPie

This is the repostory of putting personal *public* computing science related tutorials, notes and examples. Any example program or code fregments used in [jaiyalas.github.io](http://jaiyalas.github.io) should be placed in this place. 

## Branchs

* [master] - for developing
* <del>develop</del>
* [release] - for finished materials

## Warning and TODO

This repostory has been designed as two copies. This first one contains all branches and hosts on bitbucket. The onther one was just for repeasing, therefore hosted on github. The push.hs is a haskell program for easily push to either bitbucket or github with pre-written configurations. This whole pushing design, however, is broken right now since I removed the repository on github. 

### TODO

* add repo on github
* set .git/config
* rewrite push.sh
