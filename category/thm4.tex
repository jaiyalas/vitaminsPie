\documentclass{article}

\usepackage{color, latexsym}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{tikz}
\usetikzlibrary{matrix,arrows,intersections}
\tikzset{node distance=2cm, auto}

\newcommand{\cata}[1]{(\![{#1}]\!)}
\newcommand{\types}{{\ ::\ }}
\newcommand{\op}{\operatorname}
\newcommand{\pend}{\begin{flushright}$\square$\end{flushright}}

\title{Things about Natural Transformation}
\author{Yun-Yan Chi}

\begin{document}

\maketitle

\section{Preliminaries}

% ========== Functor ==========

Let us start with the definition of category, but, for simplicity, we will omit some unnecessary details. A \textit{category} consists of two collections: objects and arrows. Base on the foregoing description of category, a \textit{functor} is a mapping between two category. We write $\mathtt{F} \types \mathcal{C}_1 \to \mathcal{C}_2$ as a functor and $\mathtt{F}$ maps any object $A$ and arrow $f \types C \to D$ in $\mathcal{C}_1$ to $\mathtt{F} A$ and $\mathtt{F} f \types \mathtt{F} C \to \mathtt{F} D$ in $\mathcal{C}_2$.

\begin{center}
\begin{tikzpicture}
  \node (A)           {$A$};
  \node (FA)  [right of=A] {$\mathtt{F} A$};
  \node (B)   [below of=A] {$B$};
  \node (FB)    [below of=FA] {$\mathtt{F} B$};

  \draw[->] (A)  to node {$\mathtt{F}$} (FA);
  \draw[->] (B) to node [swap] {$\mathtt{F}$} (FB);

  \draw[->] (FA) to node {$\mathtt{F} f$} (FB);
  \draw[->] (A)  to node [swap] {$f$} (B);
\end{tikzpicture}
\end{center}

There are several different points of view in functor. First of all, a functor is a arrow between two categories as mentioned above. One may also regard a functor as a structure introduction to construct complicate structure. Finally, functor could also be viewed as an approach to construct a new category from original one because a functor can transform all of objects and arrows in one category. 

\pend

% ========== Catamorphism ==========

Let us omit the definitions of \textit{$\mathtt{F}$-Algebra} and \textit{Catamorphism}.

\begin{center}
\begin{tikzpicture}
  \node (FmuF)           {$\mathtt{F}\:\mu\mathtt{F}$};
  \node (muF)  [right of=FmuF] {$\mu\mathtt{F}$};
  \node (FA)   [below of=FmuF] {$\mathtt{F} A$};
  \node (A)    [below of=muF] {$A$};

  \draw[->] (FmuF) to node (x) {${in}_{\mathtt{F}}$} (muF);
  \draw[->] (FA)   to node (y) [swap] {$f$} (A);

  \draw[->] (FmuF) to node [swap] {$\mathtt{F} \cata{f}$} (FA);
  \draw[->] (muF) to node {$\cata{f}$} (A);
\end{tikzpicture}
\end{center}

\pend

% ========== Transformation ==========

A {\it transformation}, $\eta \types \mathtt{G} \dot{\to} \mathtt{H}$, is a mapping between two functors. One way to understand the capability of transformations is to regard it as structure transformer. For example, $\mathnormal{List}\:A \to \mathnormal{Tree}\:A$.



% A {\it transformation} $\Phi$ on category $\mathcal{C}_1$ is a mapping that maps any object $A$ in $\mathcal{C}_1$ to an arrow $f \types X \to Y$ for some objects $X$ and $Y$. Objects $X$ and $Y$ must be in a same category which may or may not be $\mathcal{C}_1$. To indicate the unknown category, we define two functors $\mathtt{F}$ and $\mathtt{G}$ that satisfy $\mathtt{F} A = X$ and $\mathtt{G} A = Y$. To explicitly indicate the relationship between arrow $f$ and object $A$, we also use $\Phi_{A}$ to represent $f$.

% \begin{center}
% \begin{tikzpicture}
%   \node (A)               {$A$};
%   \node (X) [right of=GA, above of=A] {$X$};
%   \node (Y) [right of=GA, below of=A] {$Y$};
%   \draw[->] (X) to node (f) [swap] {$f$} (Y);
%   \draw[|->] (A) to node {$\phi$} (f);
%   \node (FA) [right of=X] {$\mathtt{F} A$};
%   \node (GA) [right of=Y] {$\mathtt{G} A$};
%   \draw[-,double] (X) to (FA);
%   \draw[-,double] (Y) to (GA);
%   \draw[->] (FA) to node (phi0) [swap] {$\phi_{A}$} (GA);
%   \draw[-,double] (f.east)+(4pt,0) to (phi0);
% \end{tikzpicture}
% \end{center}

% \paragraph{Note} ``For all $A$ in $\mathcal{C}_1$'' means functor must be ``total''. However, since (1) there is no restriction on funtor for totality; (2) any functor is a homomorphism between two categories, now the problem is: whether a homomorphism must be total?

For two functors $\mathtt{G}$ and $\mathtt{H}$, both of which has type $\mathcal{A}\to\mathcal{B}$ for some categories $\mathcal{A}$ and $\mathcal{B}$, a {\it transformation} $\Phi$ from $\mathtt{G}$ to $\mathtt{H}$ is a collection of arrows $\Phi_\alpha \types \mathtt{G}\ \alpha \to \mathtt{H}\ \alpha$ for all $\alpha$ is an object in $\mathcal{A}$. 

% Follow the definition of $\op{\mathtt{F}-algebra}$, for functors $\mathtt{G}$ and $\mathtt{H}$ we have two corresponing algebras $\op{\mathtt{G}-algebra}$ and $\op{\mathtt{H}-algebra}$ as follows.

For any function $f\types\ A \to B$ and two functors $\mathtt{G}$ and $\mathtt{H}$ we obtain $\mathtt{G}\ f\types \mathtt{G}\ A \to \mathtt{G}\ B$ and $\mathtt{H}\ f\types\mathtt{H}\ A \to \mathtt{H}\ B$ as follows.



\begin{center}
\begin{tikzpicture}[
  back line/.style={densely dotted},
  cross line/.style={preaction={draw=white, -, line width=6pt}},
  descr/.style={fill=white,inner sep=2.5pt}]

  \node (A)              {$A$};
  \node (B) [below of=A,node distance=5cm] {$B$};

  \node (GA) [right of=A,node distance=4cm] {$\mathtt{G} A$};
  \node (GB) [below of=GA,node distance=5cm] {$\mathtt{G} B$};

  \node (HA) [right of=A,above of=A] {$\mathtt{H} A$};
  \node (HB) [below of=HA,node distance=5cm] {$\mathtt{H} B$};

  \draw[->] (A) to node [swap] {$f$} (B);
  \draw[->,thick] (GA) to node {$\mathtt{G} f$} (GB);
  \draw[->,thick] (HA) to node {$\mathtt{H} f$} (HB);

  \draw[->,cross line] (A) to node [font=\scriptsize,descr,below=-5pt] {$\mathtt{G}$} (GA);
  \draw[->] (A) to node [font=\scriptsize,descr,right=-4pt] {$\mathtt{H}$} (HA);

  \draw[->] (B) to node [font=\scriptsize,descr,below=-5pt] {$\mathtt{G}$} (GB);
  \draw[->] (B) to node [font=\scriptsize,descr,right=-4pt] {$\mathtt{H}$} (HB);

  \draw[->,thick] (GA) to node (phia) [font=\scriptsize,descr,right=-4pt] {$\Phi_{A}$} (HA);
  \draw[->,thick] (GB) to node (phib) [font=\scriptsize,descr,right=-4pt] {$\Phi_{B}$} (HB);

  \draw[|->,cross line] (A)+(10pt,5pt) to node [font=\scriptsize,descr,right=-4pt] {$\Phi$} (phia);
  \draw[|->] (B)+(10pt,5pt) to node [font=\scriptsize,descr,right=-4pt] {$\Phi$} (phib);
\end{tikzpicture}
\end{center}


\begin{center}
\begin{tikzpicture}
  \node (GA)               {$\mathtt{G}\ A$};
  \node (GB) [right of=GA] {$\mathtt{G}\ B$};
  \node (HA) [below of=GA] {$\mathtt{H}\ A$};
  \node (HB) [below of=GB] {$\mathtt{H}\ B$};

  \draw[->] (GA) to node {$\mathtt{G}\ f$} (GB);
  \draw[->] (HA) to node [swap] {$\mathtt{H}\ f$} (HB);

  \draw[->] (GA) to node [swap] {$\Phi_{A}$} (HA);
  \draw[->] (GB) to node {$\Phi_{B}$} (HB);
\end{tikzpicture}
\end{center}

\pend

\paragraph{Exercise:} 

\begin{center}
\begin{tikzpicture}[scale=2]
  \draw[step=.5cm,gray!20,very thin] (0,0) grid (2.4,2.4);
  \draw[->] (-0.5,0) -- (2.5,0);
  \draw[->] (0,-0.5) -- (0,2.5);

  \draw (0.3,0.4) circle (0.5pt);
  \draw (0.4,0.8) circle (0.5pt);
  \draw (0.6,1.4) circle (0.5pt);
  \draw (0.6,0.7) circle (0.5pt);
  \draw (0.7,2.2) circle (0.5pt);
  \draw (1.4,2.4) circle (0.5pt);
  \draw (1.5,0.4) circle (0.5pt);
  \draw (1.7,1.8) circle (0.5pt);
  \draw (1.7,0.4) circle (0.5pt);
  \draw (2.2,1.6) circle (0.5pt);
  \draw (2.3,0.1) circle (0.5pt);

  \draw[-,thick,orange!30] (1.1,-0.2) to (1.1,2.5);

  \draw[-,dashed,thick,blue!30] (-0.2,2.1) to (2.5,2.1);
  \draw[->,thick,blue!30] (2.4,2.3) to (2.4,1.9);
\end{tikzpicture}
\end{center}

\end{document}

