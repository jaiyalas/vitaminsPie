module Main where 
  import Control.Arrow

  -- class Arrow a => ArrowLoop a where
  --     loop :: a (b,d) (c,d) -> a b c
  -- instance ArrowLoop (->) where
      -- loop f b = let (c,d) = f (b,d) 
      --   in c

  test (x, y) = (drop (y - 2) x, length x)

  main = do
    putStrLn $ loop test "Hello world"

  -- repmin :: Ord a => Tree a -> Tree a
  -- repmin = trace repIImin
 
  -- repIImin :: Ord a => Tree a -> a -> (Tree a, a)
  -- repIImin (Leaf minval) rep = (Leaf rep, minval)
  -- repIImin (Branch left right) rep = let (left', min_left) = repIImin left rep
  --                                        (right', min_right) = repIImin right rep
  --                                    in (Branch left' right', min min_left min_right)

