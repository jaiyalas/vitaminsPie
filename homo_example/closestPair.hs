
class Pos a where
  xy :: a -> Int

class Pos a => Dir a where
  west :: a -> a -> Bool
  east :: a -> a -> Bool
  north :: a -> a -> Bool
  south :: a -> a -> Bool

data Point = P Int Int deriving Show

instance Pos Point where
  xy (P x y) = x + y

instance Dir Point where
  west (P x0 y0) (P x1 y1) 
    | x0 >= x1  = True
    | otherwise = False
  east (P x0 y0) (P x1 y1) 
    | x0 <= x1  = True
    | otherwise = False
  north (P x0 y0) (P x1 y1) 
    | y0 <= y1  = True
    | otherwise = False
  south (P x0 y0) (P x1 y1) 
    | y0 >= y1  = True
    | otherwise = False

inf :: Int
inf = maxBound

es = (zipWith (P) [0..4] [1..5]) ++ (zipWith (P) [2..14] ([1,3..9]++[5..1]))

-- cross
(><) :: (a -> c) -> (b -> d) -> (a, b) -> (c, d)
f >< g = \(a, b) -> (f a, g b)

--split
(<>) :: (a -> b) -> (a -> c) -> a -> (b, c)
f <> g = \a -> (f a, g a)

app :: ((a -> b), a) -> b
app (f, a) = f a

cons :: (a, [a]) -> [a]
cons = uncurry (:)

oneCls :: Dir a => (a,[a]) -> (a,(a,Int))
-- oneCls (a,xs) = (a, (mini.map (id <> xy).filter (ne a)) xs)
--   where ne b t = (north b t) && (east b t)
--         mini = foldr (\(b,n) (t,m) -> if m <= n then (t,m) else (b,n)) (a,inf)

-- oneCls (a,xs) = (a, (mini.meme (ne a)) xs)
--   where ne b t = (north b t) && (east b t)
--         meme b = foldr (\x     hs    -> if b x    then (x,xy x):hs else hs)    []
--         mini   = foldr (\(b,n) (t,m) -> if m <= n then (t,m)       else (b,n)) (a,inf)

-- oneCls (a,xs) = (a, (xd (ne a)) xs)
--   where ne b t = (north b t) && (east b t)
--         xd pred = foldr (\b (t,m) -> let n = xy b in 
--           if (pred b) && n <= m then (b,n) else (t,m)) (a,inf)

oneCls = fst <> (app.((xd.(ne <> id)) >< id))
  where ne b t = (north b t) && (east b t)
        xd (pred, a) = foldr (\b (t,m) -> let n = xy b in 
          if (pred b) && n <= m then (b,n) else (t,m)) (a,inf)


--   map f 
-- = foldr (curry (cons . (f >< id))) []

--   map (id <> xy)
-- = foldr (curry (cons . ((id <> xy) >< id))) []

--   filter b 
-- = foldr (\x hs -> if b x then (x:hs) else hs) []

--   map (id <> xy) . filter (ne a)
-- = foldr (\x hs -> if (ne a) x then (x,xy x):hs else hs)



-- dist :: [a] -> [(a,[a])]
-- dist (xs ++ ys) 
--   = map (id >< (++ map fst h_ys)) h_xs ++ map (id >< (++ map fst h_xs)) h_ys
--   = map (id >< (++ map fst h_ys)) h_xs ++ h_ys
--   = map (id >< (++ ys)) h_xs ++ map (id >< (++ xs)) h_ys
--   = map (id >< (++ ys)) h_xs ++ h_ys

dist_pw :: Dir a => [a] -> [(a,[a])]
dist_pw [] = []
dist_pw (x:xs) = (x,xs) : (map (id >< (x:)) $ dist_pw xs)

dist :: Dir a => [a] -> [(a,[a])]
dist = foldr (\x hs -> (x,(map fst hs)) : map (id >< (x:)) hs) []

-- allCls = map oneCls . dist


-- assume that all samples are sorted by X and Y.

allCls_ne :: Dir a  => [a] -> [(a,(a,Int))]
allCls_ne = map oneCls . dist

--   map oneCls . (foldr (\x hs -> (x,(map fst hs)) : map (id >< (x:)) hs) [])
-- = (foldr (\x hs -> oneCls x : hs) []) . (foldr (\x hs -> (x,(map fst hs)) : map (id >< (x:)) hs) [])
-- = (foldr (\x hs -> (oneCls (x,(map fst hs))) : map (id >< (x:)) hs) [])

allCls_ne' :: Dir a  => [a] -> [(a,(a,Int))]
allCls_ne' = foldr (\x hs -> (oneCls (x,(map fst hs))) : map (oneCls . (id >< ((\a -> x:[a]).fst))) hs) []

es2 = [P 0 1,
  P 1 2,
  P 2 1,
  P 2 2,
  P 3 3,
  P 3 4,
  P 4 5,
  P 5 5,
  P 5 7,
  P 6 9]



